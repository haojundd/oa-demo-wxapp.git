// pages/notice/detail/index.ts
import NoticeApi from "../../../api/NoticeApi"
import Toast from "tdesign-miniprogram/toast/index"

Page({

  /**
   * 页面的初始数据
   */
  data: {},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options: any) {
    let id = options.id
    NoticeApi.getById(id).then((res: any) => {
      this.setData({ ...res.data[0] })
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
})
