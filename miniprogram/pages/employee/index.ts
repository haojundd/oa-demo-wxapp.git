import UserApi from "../../api/UserApi"
import Toast from "tdesign-miniprogram/toast"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userId: undefined,
    version: 0,
    nickname: "",
    phone: "",
    email: "",
    avatar: "https://tdesign.gtimg.com/miniprogram/images/avatar1.png"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options: { userId: number } | any) {
    UserApi.get(options.userId).then((res: any) => {
      this.setData({ ...res.data[0] })
    }).catch(e => {
      Toast({
        context: this,
        selector: '#t-toast',
        message: e,
      })
    })
  },
})
