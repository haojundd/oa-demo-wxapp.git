import Request from "./NetConfig"

export default class AuthApi {

  static login(data: { userId: string, password: string }) {
    return Request.post("/login", data)
  }

  static logout() {
    return Request.post("/logout")
  }

  static current() {
    return Request.get("/current")
  }

}
