import request from './NetConfig'

/**
 * 考勤表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class AttendanceApi {
  static page(data: Attendance) {
    let { index, size, ...vo } = data
    return request.post(`/attendance/page/${index}/${size}`, vo)
  }

  static list(data: Attendance) {
    return request.post('/attendance/list', data)
  }

  static getById(id: number) {
    return request.get(`/attendance/${id}`)
  }

  static save(data: Attendance) {
    return request.post('/attendance', data)
  }

  static update(data: Attendance) {
    return request.put('/attendance', data)
  }

  static delete(ids: number[]) {
    return request.post('/attendance/delete', ids)
  }

  static getSignTime() {
    return request.get('/attendance/get-sign-time')
  }

  static signIn(data: Attendance) {
    return request.post('/attendance/sign-in', data)
  }

  static handle(data: Attendance) {
    return request.post('/attendance/handle', data)
  }

  static uploadAnnex(path: string) {
    return request.upload('/attendance/uploadAnnex', path, 'annex')
  }
}
