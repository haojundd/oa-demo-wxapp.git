import request from './NetConfig';

/**
 * 用户信息 前端接口
 *
 * @author haojun
 * @since 2022-06-12
 */
export default class UserApi {
  static page(data: User) {
    let { index, size, ...vo } = data;
    return request.post(`/user/page/${index}/${size}`, vo);
  }

  static get(id: number) {
    return request.get(`/user/${id}`);
  }

  static save(data: User) {
    return request.post('/user', data);
  }

  static update(data: User) {
    return request.put('/user', data);
  }

  static delete(ids: number[]) {
    return request.post('/user/delete', ids);
  }

  static changeAvatar(path: string) {
    return request.upload('/user/changeAvatar', path, 'avatar');
  }

  static changePwd(data: object) {
    return request.post('/user/changePwd', data);
  }

  static resetPwd(data: object) {
    return request.post('/user/resetPwd', data);
  }
}
